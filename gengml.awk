#! /usr/bin/gawk -f

BEGIN {RS = "~@"; printf "graph [\n";}

/^[0-9]/{

s = sprintf("\n node [id %s label \"  ", $1);

for(i = 2; i < NF; i++)

s = s sprintf("%s | ", $i);

s = s sprintf("%s  \"", $i);

$0 = s;

while (/([a-zA-Z0-9]+):@([0-9]+)/){

format = sprintf("\\1 \\3", $1);

$0 = gensub(/([a-zA-Z0-9]+):@([0-9]+)(.*)$/, format, "g");

};

printf " %s  ]", $0;

}

END {print "\n"}
