#! /usr/bin/gawk -f

BEGIN {RS = "~@"; printf "\n";}

/^[0-9]/{
ss=$1;

s = sprintf("\n# node %s ", $1);

for(i = 2; i < NF; i++)

s = s sprintf("%s | ", $i);

s = s sprintf("%s", $i);

$0 = s;

while (/([a-zA-Z0-9]+):@([0-9]+)/){

format = sprintf("\\1 \\3\n edge [ source %s target \\2 ]", ss);

$0 = gensub(/([a-zA-Z0-9]+):@([0-9]+)(.*)$/, format, "g");

};

printf " %s", $0;

}

END {print "\n]\n"}
